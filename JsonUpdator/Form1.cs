﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Http;
using System.Xml;
using System.IO;
using System.Data.OleDb;
using Newtonsoft;
using Newtonsoft.Json;

namespace JsonUpdator
{
    public partial class Form1 : MetroForm
    {
        private const string REPLCAE_LOCALE = "[LOCALE]";
        private string cURL = @"https://api.getlocalization.com/ikea/api/translations/file/popup-texts.xml/" + REPLCAE_LOCALE + "/";
        public Form1()
        {
            InitializeComponent();
        }

        private async void metroButton1_Click(object sender, EventArgs e)
        {

            var item = "";// this.metroComboBox1.Text;
            if (String.IsNullOrEmpty(item))
            {
                MessageBox.Show("Select Locale");
                return;
            }


            var client = new HttpClient();


            var byteArray = Encoding.ASCII.GetBytes("mohammad.kashif:bdQFfe");
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));


            var url = cURL.Replace(REPLCAE_LOCALE, item);
            // Get the response.
            var responseStream = await client.GetStreamAsync(url);
            //XmlDocument document = new XmlDocument();

            //using (XmlReader reader = XmlReader.Create(responseStream))
            //{
            //    document.Load(responseStream);
            //}


            var ds = new DataSet();
            ds.ReadXml(responseStream);

            // metroGrid1.DataSource = ds.DefaultViewManager;
            //ds.DefaultViewManager
            //metroGrid1

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string filePath = @"\\ITSEELM-NT0028\DIGER2$\Desktop\prerna\NewWork\JsonUpdator(1)\JsonUpdator\JsonUpdator\samples\convertcsv.xlsx";
            string SourceConstr = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + @";Extended Properties= ""Excel 12.0 Macro;HDR=Yes;ImpoertMixedTypes=Text;TypeGuessRows=0""";
            OleDbConnection con = new OleDbConnection(SourceConstr);
            string query = "Select * from [Sheet 1$]";
            try
            {
                con.Open();
                OleDbDataAdapter data = new OleDbDataAdapter(query, con);
                DataTable dtExcel = new DataTable();
                data.Fill(dtExcel);
                foreach (DataRow dr in dtExcel.Rows)
                {
                    ListViewItem lvi = new ListViewItem(dr["Locale"].ToString());
                    listAvailableLocales.Items.Add(lvi);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                con.Close();
            }
        }

        private void btnAddLocales_Click(object sender, EventArgs e)
        {
            foreach (Object selecteditem in listAvailableLocales.SelectedItems)
            {
                listAvailableLocales.Items.Remove((ListViewItem)selecteditem);
                listSelectedLocales.Items.Add((ListViewItem)selecteditem);
            }
        }

        private void btnRemoveLocales_Click(object sender, EventArgs e)
        {

            foreach (Object selecteditem in listSelectedLocales.SelectedItems)
            {
                listSelectedLocales.Items.Remove((ListViewItem)selecteditem);
                listAvailableLocales.Items.Add((ListViewItem)selecteditem);
            }
        }

        private void btnFileSourceBrowser_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderDlg = new FolderBrowserDialog();
            if (folderDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txtFileSourceLocation.Text = folderDlg.SelectedPath;

            }
            DisplayFolder(folderDlg.SelectedPath);

        }
        public void DisplayFolder(string folderPath)
        {
            string targetPath = txtFileDesitnationLocation.Text;
            string allowedExtention = "*.json";
            List<String> itemSelected = new List<String>();

            foreach (var selectedRow in listSelectedLocales.SelectedItems)
            {
                itemSelected.Add(((ListViewItem)selectedRow).Text);
            }
            
            string[] files = System.IO.Directory.GetFiles(folderPath, allowedExtention);

            for (int x = 0; x < files.Length; x++)
            {
                foreach(string val in itemSelected)
                {
                    if(files[x].Contains(val))
                    {
                        listLogs.Items.Add(files[x]);
                        // this code use to copy files from source to destination.
                        string fileName = System.IO.Path.GetFileName(files[x]);
                        string destFile = System.IO.Path.Combine(targetPath, fileName);
                        System.IO.File.Copy(files[x], destFile, true);
                    }
                }
            }
        }

        //public void copyToDestination()
        //{
        //    foreach ( s in listLogs)
        //    {
        //        // Use static Path methods to extract only the file name from the path.
        //        fileName = System.IO.Path.GetFileName(s);
        //        destFile = System.IO.Path.Combine(targetPath, fileName);
        //        System.IO.File.Copy(s, destFile, true);
        //    }
        //}
        private void btnFileDestinationBrowser_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderDlg = new FolderBrowserDialog();
            if (folderDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txtFileDesitnationLocation.Text = folderDlg.SelectedPath;

            }

        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            InputJson jsonObject = JsonConvert.DeserializeObject<InputJson>(File.ReadAllText(@"\\ITSEELM-NT0028\DIGER2$\Desktop\prerna\NewWork\JsonUpdator(1)\JsonUpdator\JsonUpdator\samples\dk_da.json"));
        }
    }
}
