﻿namespace JsonUpdator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.comboType = new MetroFramework.Controls.MetroComboBox();
            this.listAvailableLocales = new MetroFramework.Controls.MetroListView();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.listSelectedLocales = new MetroFramework.Controls.MetroListView();
            this.btnAddLocales = new MetroFramework.Controls.MetroButton();
            this.btnUpdate = new MetroFramework.Controls.MetroButton();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.txtFileSourceLocation = new MetroFramework.Controls.MetroTextBox();
            this.btnFileSourceBrowser = new MetroFramework.Controls.MetroButton();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.btnFileDestinationBrowser = new MetroFramework.Controls.MetroButton();
            this.txtFileDesitnationLocation = new MetroFramework.Controls.MetroTextBox();
            this.btnRemoveLocales = new MetroFramework.Controls.MetroButton();
            this.radioBtnPlatformIOS = new MetroFramework.Controls.MetroRadioButton();
            this.radioBtnPlatformAndroid = new MetroFramework.Controls.MetroRadioButton();
            this.radioBtnPlatformBoth = new MetroFramework.Controls.MetroRadioButton();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.txtBoxVersion = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.radioBtnMergeTypeCleanAndCreate = new MetroFramework.Controls.MetroRadioButton();
            this.metroRadioButton5 = new MetroFramework.Controls.MetroRadioButton();
            this.radioBtnMergeTypeMerge = new MetroFramework.Controls.MetroRadioButton();
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.metroPanel2 = new MetroFramework.Controls.MetroPanel();
            this.progressBar = new MetroFramework.Controls.MetroProgressBar();
            this.listLogs = new MetroFramework.Controls.MetroListView();
            this.metroPanel1.SuspendLayout();
            this.metroPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(23, 61);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(40, 19);
            this.metroLabel2.TabIndex = 4;
            this.metroLabel2.Text = "Type:";
            // 
            // comboType
            // 
            this.comboType.FormattingEnabled = true;
            this.comboType.ItemHeight = 23;
            this.comboType.Items.AddRange(new object[] {
            "App Expiry",
            "Lock App",
            "Forced Upgrade",
            "Optional Upgrade"});
            this.comboType.Location = new System.Drawing.Point(23, 83);
            this.comboType.Name = "comboType";
            this.comboType.Size = new System.Drawing.Size(351, 29);
            this.comboType.TabIndex = 1;
            this.comboType.UseSelectable = true;
            // 
            // listAvailableLocales
            // 
            this.listAvailableLocales.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.listAvailableLocales.FullRowSelect = true;
            this.listAvailableLocales.Location = new System.Drawing.Point(23, 147);
            this.listAvailableLocales.Name = "listAvailableLocales";
            this.listAvailableLocales.OwnerDraw = true;
            this.listAvailableLocales.Size = new System.Drawing.Size(149, 208);
            this.listAvailableLocales.TabIndex = 5;
            this.listAvailableLocales.UseCompatibleStateImageBehavior = false;
            this.listAvailableLocales.UseSelectable = true;
            this.listAvailableLocales.View = System.Windows.Forms.View.List;
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(23, 125);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(89, 19);
            this.metroLabel3.TabIndex = 4;
            this.metroLabel3.Text = "Select Locales";
            // 
            // listSelectedLocales
            // 
            this.listSelectedLocales.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.listSelectedLocales.FullRowSelect = true;
            this.listSelectedLocales.Location = new System.Drawing.Point(229, 147);
            this.listSelectedLocales.Name = "listSelectedLocales";
            this.listSelectedLocales.OwnerDraw = true;
            this.listSelectedLocales.Size = new System.Drawing.Size(145, 208);
            this.listSelectedLocales.TabIndex = 5;
            this.listSelectedLocales.UseCompatibleStateImageBehavior = false;
            this.listSelectedLocales.UseSelectable = true;
            this.listSelectedLocales.View = System.Windows.Forms.View.List;
            // 
            // btnAddLocales
            // 
            this.btnAddLocales.Location = new System.Drawing.Point(178, 205);
            this.btnAddLocales.Name = "btnAddLocales";
            this.btnAddLocales.Size = new System.Drawing.Size(45, 23);
            this.btnAddLocales.TabIndex = 6;
            this.btnAddLocales.Text = ">>";
            this.btnAddLocales.UseSelectable = true;
            this.btnAddLocales.Click += new System.EventHandler(this.btnAddLocales_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(249, 381);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(273, 23);
            this.btnUpdate.TabIndex = 6;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseSelectable = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(452, 246);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(184, 19);
            this.metroLabel1.TabIndex = 4;
            this.metroLabel1.Text = "Select Source JSON Location: ";
            // 
            // txtFileSourceLocation
            // 
            // 
            // 
            // 
            this.txtFileSourceLocation.CustomButton.Image = null;
            this.txtFileSourceLocation.CustomButton.Location = new System.Drawing.Point(241, 1);
            this.txtFileSourceLocation.CustomButton.Name = "";
            this.txtFileSourceLocation.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtFileSourceLocation.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtFileSourceLocation.CustomButton.TabIndex = 1;
            this.txtFileSourceLocation.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtFileSourceLocation.CustomButton.UseSelectable = true;
            this.txtFileSourceLocation.CustomButton.Visible = false;
            this.txtFileSourceLocation.Lines = new string[0];
            this.txtFileSourceLocation.Location = new System.Drawing.Point(452, 268);
            this.txtFileSourceLocation.MaxLength = 32767;
            this.txtFileSourceLocation.Name = "txtFileSourceLocation";
            this.txtFileSourceLocation.PasswordChar = '\0';
            this.txtFileSourceLocation.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtFileSourceLocation.SelectedText = "";
            this.txtFileSourceLocation.SelectionLength = 0;
            this.txtFileSourceLocation.SelectionStart = 0;
            this.txtFileSourceLocation.ShortcutsEnabled = true;
            this.txtFileSourceLocation.Size = new System.Drawing.Size(263, 23);
            this.txtFileSourceLocation.TabIndex = 7;
            this.txtFileSourceLocation.UseSelectable = true;
            this.txtFileSourceLocation.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtFileSourceLocation.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // btnFileSourceBrowser
            // 
            this.btnFileSourceBrowser.Location = new System.Drawing.Point(721, 268);
            this.btnFileSourceBrowser.Name = "btnFileSourceBrowser";
            this.btnFileSourceBrowser.Size = new System.Drawing.Size(28, 23);
            this.btnFileSourceBrowser.TabIndex = 6;
            this.btnFileSourceBrowser.Text = "...";
            this.btnFileSourceBrowser.UseSelectable = true;
            this.btnFileSourceBrowser.Click += new System.EventHandler(this.btnFileSourceBrowser_Click);
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(452, 309);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(208, 19);
            this.metroLabel4.TabIndex = 4;
            this.metroLabel4.Text = "Select Destination JSON Location: ";
            // 
            // btnFileDestinationBrowser
            // 
            this.btnFileDestinationBrowser.Location = new System.Drawing.Point(721, 332);
            this.btnFileDestinationBrowser.Name = "btnFileDestinationBrowser";
            this.btnFileDestinationBrowser.Size = new System.Drawing.Size(28, 23);
            this.btnFileDestinationBrowser.TabIndex = 6;
            this.btnFileDestinationBrowser.Text = "...";
            this.btnFileDestinationBrowser.UseSelectable = true;
            this.btnFileDestinationBrowser.Click += new System.EventHandler(this.btnFileDestinationBrowser_Click);
            // 
            // txtFileDesitnationLocation
            // 
            // 
            // 
            // 
            this.txtFileDesitnationLocation.CustomButton.Image = null;
            this.txtFileDesitnationLocation.CustomButton.Location = new System.Drawing.Point(241, 1);
            this.txtFileDesitnationLocation.CustomButton.Name = "";
            this.txtFileDesitnationLocation.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtFileDesitnationLocation.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtFileDesitnationLocation.CustomButton.TabIndex = 1;
            this.txtFileDesitnationLocation.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtFileDesitnationLocation.CustomButton.UseSelectable = true;
            this.txtFileDesitnationLocation.CustomButton.Visible = false;
            this.txtFileDesitnationLocation.Lines = new string[0];
            this.txtFileDesitnationLocation.Location = new System.Drawing.Point(452, 332);
            this.txtFileDesitnationLocation.MaxLength = 32767;
            this.txtFileDesitnationLocation.Name = "txtFileDesitnationLocation";
            this.txtFileDesitnationLocation.PasswordChar = '\0';
            this.txtFileDesitnationLocation.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtFileDesitnationLocation.SelectedText = "";
            this.txtFileDesitnationLocation.SelectionLength = 0;
            this.txtFileDesitnationLocation.SelectionStart = 0;
            this.txtFileDesitnationLocation.ShortcutsEnabled = true;
            this.txtFileDesitnationLocation.Size = new System.Drawing.Size(263, 23);
            this.txtFileDesitnationLocation.TabIndex = 7;
            this.txtFileDesitnationLocation.UseSelectable = true;
            this.txtFileDesitnationLocation.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtFileDesitnationLocation.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // btnRemoveLocales
            // 
            this.btnRemoveLocales.Location = new System.Drawing.Point(178, 242);
            this.btnRemoveLocales.Name = "btnRemoveLocales";
            this.btnRemoveLocales.Size = new System.Drawing.Size(45, 23);
            this.btnRemoveLocales.TabIndex = 6;
            this.btnRemoveLocales.Text = "<<";
            this.btnRemoveLocales.UseSelectable = true;
            this.btnRemoveLocales.Click += new System.EventHandler(this.btnRemoveLocales_Click);
            // 
            // radioBtnPlatformIOS
            // 
            this.radioBtnPlatformIOS.AutoSize = true;
            this.radioBtnPlatformIOS.Location = new System.Drawing.Point(9, 7);
            this.radioBtnPlatformIOS.Name = "radioBtnPlatformIOS";
            this.radioBtnPlatformIOS.Size = new System.Drawing.Size(41, 15);
            this.radioBtnPlatformIOS.TabIndex = 8;
            this.radioBtnPlatformIOS.Text = "iOS";
            this.radioBtnPlatformIOS.UseSelectable = true;
            // 
            // radioBtnPlatformAndroid
            // 
            this.radioBtnPlatformAndroid.AutoSize = true;
            this.radioBtnPlatformAndroid.Checked = true;
            this.radioBtnPlatformAndroid.Location = new System.Drawing.Point(64, 7);
            this.radioBtnPlatformAndroid.Name = "radioBtnPlatformAndroid";
            this.radioBtnPlatformAndroid.Size = new System.Drawing.Size(66, 15);
            this.radioBtnPlatformAndroid.TabIndex = 8;
            this.radioBtnPlatformAndroid.TabStop = true;
            this.radioBtnPlatformAndroid.Text = "Android";
            this.radioBtnPlatformAndroid.UseSelectable = true;
            // 
            // radioBtnPlatformBoth
            // 
            this.radioBtnPlatformBoth.AutoSize = true;
            this.radioBtnPlatformBoth.Location = new System.Drawing.Point(138, 7);
            this.radioBtnPlatformBoth.Name = "radioBtnPlatformBoth";
            this.radioBtnPlatformBoth.Size = new System.Drawing.Size(48, 15);
            this.radioBtnPlatformBoth.TabIndex = 8;
            this.radioBtnPlatformBoth.Text = "Both";
            this.radioBtnPlatformBoth.UseSelectable = true;
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(452, 61);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(103, 19);
            this.metroLabel5.TabIndex = 4;
            this.metroLabel5.Text = "Select Platforms";
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(452, 183);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(97, 19);
            this.metroLabel6.TabIndex = 4;
            this.metroLabel6.Text = "Specify Version";
            // 
            // txtBoxVersion
            // 
            // 
            // 
            // 
            this.txtBoxVersion.CustomButton.Image = null;
            this.txtBoxVersion.CustomButton.Location = new System.Drawing.Point(278, 1);
            this.txtBoxVersion.CustomButton.Name = "";
            this.txtBoxVersion.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtBoxVersion.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtBoxVersion.CustomButton.TabIndex = 1;
            this.txtBoxVersion.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtBoxVersion.CustomButton.UseSelectable = true;
            this.txtBoxVersion.CustomButton.Visible = false;
            this.txtBoxVersion.Lines = new string[0];
            this.txtBoxVersion.Location = new System.Drawing.Point(452, 211);
            this.txtBoxVersion.MaxLength = 32767;
            this.txtBoxVersion.Name = "txtBoxVersion";
            this.txtBoxVersion.PasswordChar = '\0';
            this.txtBoxVersion.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtBoxVersion.SelectedText = "";
            this.txtBoxVersion.SelectionLength = 0;
            this.txtBoxVersion.SelectionStart = 0;
            this.txtBoxVersion.ShortcutsEnabled = true;
            this.txtBoxVersion.Size = new System.Drawing.Size(300, 23);
            this.txtBoxVersion.TabIndex = 7;
            this.txtBoxVersion.UseSelectable = true;
            this.txtBoxVersion.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtBoxVersion.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(452, 125);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(118, 19);
            this.metroLabel7.TabIndex = 9;
            this.metroLabel7.Text = "Select Merge Type";
            // 
            // radioBtnMergeTypeCleanAndCreate
            // 
            this.radioBtnMergeTypeCleanAndCreate.AutoSize = true;
            this.radioBtnMergeTypeCleanAndCreate.Checked = true;
            this.radioBtnMergeTypeCleanAndCreate.Location = new System.Drawing.Point(12, 7);
            this.radioBtnMergeTypeCleanAndCreate.Name = "radioBtnMergeTypeCleanAndCreate";
            this.radioBtnMergeTypeCleanAndCreate.Size = new System.Drawing.Size(140, 15);
            this.radioBtnMergeTypeCleanAndCreate.TabIndex = 8;
            this.radioBtnMergeTypeCleanAndCreate.TabStop = true;
            this.radioBtnMergeTypeCleanAndCreate.Text = "Clean and Create New";
            this.radioBtnMergeTypeCleanAndCreate.UseSelectable = true;
            // 
            // metroRadioButton5
            // 
            this.metroRadioButton5.AutoSize = true;
            this.metroRadioButton5.Location = new System.Drawing.Point(539, 205);
            this.metroRadioButton5.Name = "metroRadioButton5";
            this.metroRadioButton5.Size = new System.Drawing.Size(16, 0);
            this.metroRadioButton5.TabIndex = 8;
            this.metroRadioButton5.UseSelectable = true;
            // 
            // radioBtnMergeTypeMerge
            // 
            this.radioBtnMergeTypeMerge.AutoSize = true;
            this.radioBtnMergeTypeMerge.Location = new System.Drawing.Point(158, 7);
            this.radioBtnMergeTypeMerge.Name = "radioBtnMergeTypeMerge";
            this.radioBtnMergeTypeMerge.Size = new System.Drawing.Size(57, 15);
            this.radioBtnMergeTypeMerge.TabIndex = 8;
            this.radioBtnMergeTypeMerge.Text = "Merge";
            this.radioBtnMergeTypeMerge.UseSelectable = true;
            // 
            // metroPanel1
            // 
            this.metroPanel1.Controls.Add(this.radioBtnMergeTypeCleanAndCreate);
            this.metroPanel1.Controls.Add(this.radioBtnMergeTypeMerge);
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(452, 147);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(300, 29);
            this.metroPanel1.TabIndex = 11;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // metroPanel2
            // 
            this.metroPanel2.Controls.Add(this.radioBtnPlatformIOS);
            this.metroPanel2.Controls.Add(this.radioBtnPlatformAndroid);
            this.metroPanel2.Controls.Add(this.radioBtnPlatformBoth);
            this.metroPanel2.HorizontalScrollbarBarColor = true;
            this.metroPanel2.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel2.HorizontalScrollbarSize = 10;
            this.metroPanel2.Location = new System.Drawing.Point(452, 83);
            this.metroPanel2.Name = "metroPanel2";
            this.metroPanel2.Size = new System.Drawing.Size(297, 27);
            this.metroPanel2.TabIndex = 12;
            this.metroPanel2.VerticalScrollbarBarColor = true;
            this.metroPanel2.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel2.VerticalScrollbarSize = 10;
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(23, 410);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(729, 23);
            this.progressBar.TabIndex = 13;
            // 
            // listLogs
            // 
            this.listLogs.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.listLogs.FullRowSelect = true;
            this.listLogs.Location = new System.Drawing.Point(23, 439);
            this.listLogs.Name = "listLogs";
            this.listLogs.OwnerDraw = true;
            this.listLogs.Size = new System.Drawing.Size(729, 138);
            this.listLogs.TabIndex = 5;
            this.listLogs.UseCompatibleStateImageBehavior = false;
            this.listLogs.UseSelectable = true;
            this.listLogs.View = System.Windows.Forms.View.List;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.metroPanel2);
            this.Controls.Add(this.metroPanel1);
            this.Controls.Add(this.metroLabel7);
            this.Controls.Add(this.metroRadioButton5);
            this.Controls.Add(this.txtFileDesitnationLocation);
            this.Controls.Add(this.txtBoxVersion);
            this.Controls.Add(this.txtFileSourceLocation);
            this.Controls.Add(this.btnFileDestinationBrowser);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.btnFileSourceBrowser);
            this.Controls.Add(this.btnRemoveLocales);
            this.Controls.Add(this.btnAddLocales);
            this.Controls.Add(this.listSelectedLocales);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.listLogs);
            this.Controls.Add(this.listAvailableLocales);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.metroLabel6);
            this.Controls.Add(this.metroLabel5);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.comboType);
            this.Name = "Form1";
            this.Text = "JsonUpdator";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.metroPanel1.ResumeLayout(false);
            this.metroPanel1.PerformLayout();
            this.metroPanel2.ResumeLayout(false);
            this.metroPanel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroComboBox comboType;
        private MetroFramework.Controls.MetroListView listAvailableLocales;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroListView listSelectedLocales;
        private MetroFramework.Controls.MetroButton btnAddLocales;
        private MetroFramework.Controls.MetroButton btnUpdate;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroTextBox txtFileSourceLocation;
        private MetroFramework.Controls.MetroButton btnFileSourceBrowser;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroButton btnFileDestinationBrowser;
        private MetroFramework.Controls.MetroTextBox txtFileDesitnationLocation;
        private MetroFramework.Controls.MetroButton btnRemoveLocales;
        private MetroFramework.Controls.MetroRadioButton radioBtnPlatformIOS;
        private MetroFramework.Controls.MetroRadioButton radioBtnPlatformAndroid;
        private MetroFramework.Controls.MetroRadioButton radioBtnPlatformBoth;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroTextBox txtBoxVersion;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroRadioButton radioBtnMergeTypeCleanAndCreate;
        private MetroFramework.Controls.MetroRadioButton metroRadioButton5;
        private MetroFramework.Controls.MetroRadioButton radioBtnMergeTypeMerge;
        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroPanel metroPanel2;
        private MetroFramework.Controls.MetroProgressBar progressBar;
        private MetroFramework.Controls.MetroListView listLogs;
    }
}

